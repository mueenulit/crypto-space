<resources>
    <string name="app_name">ZenCrypt</string>
    <string name="encrypt_file">Encrypt File</string>
    <string name="decrypt_custom_file">Decrypt Custom File</string>
    <string name="encrypted">Encrypted</string>
    <string name="encrypt">Encrypt</string>
    <string name="decrypted">Decrypted</string>
    <string name="analyzer">Analyzer</string>
    <string name="about">About</string>
    <string name="settings">Settings</string>
    <string name="decrypt">Decrypt</string>
    <string name="share">Share</string>
    <string name="delete">Delete</string>
    <string name="password">Password</string>
    <string name="open">Open</string>
    <string name="file_name">File Name: </string>
    <string name="file_size">File Size: </string>
    <string name="encrypting">Encrypting…</string>
    <string name="decrypting">Decrypting…</string>
    <string name="loading">Loading…</string>
    <string name="something_went_wrong_empty_password">Something went wrong. Maybe empty password?</string>
    <string name="maybe_wrong_password">Action Failed! Maybe wrong password?</string>
    <string name="operation_completed_successfully">Operation completed successfully.</string>
    <string name="support_development">Support Development</string>
    <string name="zencrypt_pro_is_already_purchased">ZenCrypt pro is already purchased in this account. Thanks for supporting the app!</string>
    <string name="pro_unlocked">Pro Unlocked</string>
    <string name="you_have_already_purchased">You have already purchased ZenCrypt pro! You are awesome!</string>
    <string name="go_pro">Go Pro!</string>
    <string name="i_am_solo_developing">I am solo developing ZenCrypt. If you find this app useful, please consider unlocking the pro features.</string>
    <string name="google_play_billing_error">Google play billing error.</string>
    <string name="dark_theme">Dark Theme</string>
    <string name="choose_between_light_and_dark">Choose between light and dark theme.</string>
    <string name="zencrypt_pro_is_required">ZenCrypt pro is required to change this feature.</string>
    <string name="configuration">Configuration</string>
    <string name="enable_vibration">Enable Vibration</string>
    <string name="vibrate_after_every_action">Vibrate after every completed action (encryption/decryption).</string>
    <string name="delete_original_unencrypted">Delete Original Unencrypted</string>
    <string name="delete_original_file_after_encryption">Delete original file after encryption. Will ONLY work with files already stored within the app!</string>
    <string name="encrypted_file_extension">Encrypted File Extension</string>
    <string name="file_extension">File extension</string>
    <string name="are_you_sure_you_typed_the_correct_file_extension">Are you sure you typed correctly the file extension?</string>
    <string name="fingerprint_auth">Fingerprint Authentication</string>
    <string name="enable_fingerprint_auth">Enable Fingerprint Auth</string>
    <string name="enabling_fingerprint_auth">Enabling fingerprint authentication requires setting a custom password below!</string>
    <string name="set_a_custom_password_below">Set a custom password below.</string>
    <string name="fingerprint">Fingerprint</string>
    <string name="fingerprint_pro">Fingerprint (PRO)</string>
    <string name="fingerprint_is_not_enabled">Fingerprint is not enabled in settings!</string>
    <string name="set_custom_password">Set Custom Password</string>
    <string name="any_password_is_valid">Any password is valid. It is recommended to check password\'s integrity using the analyzer beforehand!</string>
    <string name="enter_a_password">Enter a password…</string>
    <string name="empty_password">Empty Password!</string>
    <string name="this_file_manager_is_not_supported">This file manager isn\'t supported. Please try another one.</string>
    <string name="password_set">Password Set!</string>
    <string name="about_zencrypt">About ZenCrypt</string>
    <string name="rate_zencrypt">Rate ZenCrypt</string>
    <string name="if_you_like_the_app">If you like the app, please let me now!</string>
    <string name="contact_me">Contact me</string>
    <string name="send_me_an_email">Send me an email regarding ZenCrypt. I will try and assist you as best as possible.</string>
    <string name="zencrypt_changelog_and_libraries">ZenCrypt changelog, information and open-source libraries.</string>
    <string name="source_code">Source Code</string>
    <string name="encrypted_path">Android/data/com.zestas.cryptmyfiles/files/encrypted</string>
    <string name="decrypted_path">Android/data/com.zestas.cryptmyfiles/files/decrypted</string>
    <string name="generate_random">Generate Pseudo Random</string>
    <string name="using_nulab_library">Powered by Nulab\'s zxcvbn4j library!</string>
    <string name="no_warning">No warning found.</string>
    <string name="zencrypt_is_now_open_source">Zencrypt is now fully open-source. You can view the code on GitLab!</string>
    <string name="empty_list_view_placeholder">It looks like you have no files yet…\nClick \"+\" to start encrypting!</string>
    <string name="zencrypt_description">Welcome to ZenCrypt. With this app, you can safely encrypt files, using a secure and efficient library.
        Zencrypt offers AES-256 encryption algorithm, CBC mode of operations, block padding with PKCS7,
        computationally secure random salt (of cipher block size), password stretching with PBKDF2, random IV generation on each encryption (16 bytes),
        and password analysis for strength, crack times, weakness, etc using nulab\'s zxcvbn4j library. Finally, the application does not leak (tested with LeakCanary),
        and has no internet connection permission (except for google play billing library, which is required for purchasing pro version). You can check the encryption library\'s
        source code below. Credits go to Priyank Vasa (EasyCrypt) on Github.</string>

    <string name="zencrypt_changelog"><![CDATA[ZenCrypt v3.3
        <br/>> Libraries updated.
        <br/>> Kotlin plugins updated.
        <br/>> Build Tools updated.
        <br/>> App now properly targets Android 12.]]></string>


    <!-- LIBRARY EXTRAS FOR ABOUT FRAGMENT GENERATION -->
    <!-- EasyCrypt -->
    <string name="define_plu_EasyCrypt">2018;Priyank Vasa</string>
    <string name="library_EasyCrypt_author">Priyank Vasa</string>
    <string name="library_EasyCrypt_libraryName">EasyCrypt</string>
    <string name="library_EasyCrypt_libraryDescription">Secure and efficient cryptography library for Android. EasyCrypt uses only secure implementations and all the known Crypto bugs are already dealt with properly. </string>
    <string name="library_EasyCrypt_libraryVersion">1.3.5</string>
    <string name="library_EasyCrypt_libraryWebsite">https://github.com/pvasa/EasyCrypt</string>
    <!-- you can also reference custom licenses here e.g. myLicense -->
    <string name="library_EasyCrypt_licenseIds">apache_2_0</string>
    <string name="library_EasyCrypt_isOpenSource">true</string>
    <string name="library_EasyCrypt_repositoryLink">https://github.com/pvasa/EasyCrypt</string>
    <!-- Custom variables section -->
    <string name="library_EasyCrypt_owner">Priyank Vasa</string>
    <string name="library_EasyCrypt_year">2018</string>
    <!-- Chip Navigation Bar -->
    <string name="define_plu_ChipNB">2019;Ismael Di Vita</string>
    <string name="library_ChipNB_author">Ismael Di Vita</string>
    <string name="library_ChipNB_libraryName">Chip Navigation Bar</string>
    <string name="library_ChipNB_libraryDescription">A navigation bar widget inspired on Google Bottom Navigation mixed with Chips component.</string>
    <string name="library_ChipNB_libraryVersion">1.3.4</string>
    <string name="library_ChipNB_libraryWebsite">https://github.com/ismaeldivita/chip-navigation-bar</string>
    <!-- you can also reference custom licenses here e.g. myLicense -->
    <string name="library_ChipNB_licenseIds">mit</string>
    <string name="library_ChipNB_isOpenSource">true</string>
    <string name="library_ChipNB_repositoryLink">https://github.com/ismaeldivita/chip-navigation-bar</string>
    <!-- Custom variables section -->
    <string name="library_ChipNB_owner">Ismael Di Vita</string>
    <string name="library_ChipNB_year">2019</string>
</resources>